package com.example.syn.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.ACDeviceActivator;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.service.ACBindMgr;
import com.accloud.service.ACDeviceBind;
import com.accloud.service.ACException;
import com.accloud.service.ACUserDevice;
import com.accloud.utils.PreferencesUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SYN on 2017/11/30.
 */
public class AddDeviceActivity  extends Activity implements View.OnClickListener {


    private EditText deviceId;
    private TextView back;
    private Button bind;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_adddevice);

        deviceId= (EditText) findViewById(R.id.edit_deviceId);
        back= (TextView) findViewById(R.id.add_device_back);
        bind= (Button) findViewById(R.id.bindBtn);

        back.setOnClickListener(this);
        bind.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_device_back:
                finish();
                break;
            case R.id.bindBtn:
                String physicalDeviceId=deviceId.getText().toString();
                binddevice(physicalDeviceId);
                break;

        }


    }

    private void binddevice(String physicalDeviceId) {


        AC.bindMgr().bindDevice("demo", physicalDeviceId, " ", new PayloadCallback<ACUserDevice>() {
            @Override
            public void success(ACUserDevice acUserDevice) {
                Log.i("mytag",acUserDevice.toString()+"绑定成功");
            }

            @Override
            public void error(ACException e) {
                Log.i("mytag",e+"绑定失败");
            }
        });



    }

}
