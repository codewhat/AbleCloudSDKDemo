package com.example.syn.test;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by SYN on 2017/12/1.
 */
public class Pop {
    static Toast toast=null;
    public static void popToast(Context context, String title){
        if(toast==null)
            toast=Toast.makeText(context,title,Toast.LENGTH_LONG);
        else
            toast.setText(title);
        toast.show();

    }

}
