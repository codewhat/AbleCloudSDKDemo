package com.example.syn.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.service.ACException;
import com.accloud.service.ACMsg;
import com.accloud.service.ACObject;

import java.util.ArrayList;

/**
 * Created by SYN on 2017/11/28.
 */
public class DeleteGroupActivity extends Activity {
    private ArrayList<Integer> groupIdData;
    private ListView lvDeleteGroups;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        groupIdData = new ArrayList<Integer>();
        setContentView(R.layout.layout_deletegroup);
        lvDeleteGroups = (ListView) findViewById(R.id.lvdeletegroup);
        getGroupsList();
    }


    /**
     * 向云端发送消息，获取当前用户权限下的分组列表
     */
    private void getGroupsList() {
        ACMsg req = new ACMsg();
        req.setName("getDeviceList");
        req.put("username", "18251887993");
        AC.sendToService("DEVICEDEMO", 2, req, new PayloadCallback<ACMsg>() {
            @Override
            public void success(ACMsg acMsg) {
                Log.i("mytag", "获取可以添加子分组的成功");

                //获取所有分组
                ArrayList<ACObject> groupIds = acMsg.get("groupIds");
                for (int i = 0; i < groupIds.size(); i++)
                groupIdData.add(Integer.valueOf(String.valueOf(groupIds.get(i))));

                Log.i("mytag", groupIdData.toString());
                lvDeleteGroups.setAdapter(new Adapter(getApplicationContext(), groupIdData));

            }

            @Override
            public void error(ACException e) {
                Log.i("mytag", "获取用户分组失败");
            }
        });

    }

    private class Adapter extends BaseAdapter {

        int SelectedGroupId;
        private Context context;
        private ArrayList<Integer> groupIdData;

        public Adapter(Context context, ArrayList<Integer> groupIdData) {
            this.context = context;
            this.groupIdData = groupIdData;
        }

        private class ViewHolder {
            public TextView groupId;
            public Button btnDeleteAck;
        }

        @Override
        public int getCount() {
            return groupIdData.size();
        }

        @Override
        public Object getItem(int position) {
            return groupIdData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.layout_lvedit, null);

                viewHolder = new ViewHolder();
                viewHolder.groupId = (TextView) convertView.findViewById(R.id.tvgroup);
                viewHolder.btnDeleteAck = (Button) convertView.findViewById(R.id.btnACK);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.groupId.setText(groupIdData.get(position).toString());
            viewHolder.btnDeleteAck.setText("确认删除分组");
            final int pos = position;
            viewHolder.btnDeleteAck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedGroupId = groupIdData.get(pos);
                    System.out.println("组号：" + SelectedGroupId);
                    deleteGroup(SelectedGroupId);
                }
            });
            return convertView;
        }

        /**
         * 删除指定分组
         * @param GroupId
         */

        private void deleteGroup(int GroupId) {
            ACMsg req = new ACMsg();
            req.setName("deleteGroup");
            req.put("username", "18251887993");
            req.put("groupId", GroupId);
            AC.sendToService("DEVICEDEMO", 2, req, new PayloadCallback<ACMsg>() {
                @Override
                public void success(ACMsg acMsg) {
                    Log.i("mytag", "删除分组成功");
                }

                @Override
                public void error(ACException e) {
                    Log.i("mytag", "删除分组失败" + e);
                }
            });
        }
    }
}
