package com.example.syn.test;

import android.app.Application;

import com.accloud.cloudservice.AC;
import com.accloud.utils.PreferencesUtils;

/**
 * Created by SYN on 2017/11/20.
 */
public class MyApplication extends Application{

    static final String MAJOR_DOMAIN  = "acofa";
    static final int MAJOR_DOMAIN_ID  =  5613;
    @Override
    public void onCreate() {
        super.onCreate();
        AC.init(this,MAJOR_DOMAIN,MAJOR_DOMAIN_ID,AC.TEST_MODE);
        //AC.setSendToLocalUDS("http://192.168.0.146:8080");
    }
}
