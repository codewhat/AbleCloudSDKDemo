package com.example.syn.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.cloudservice.VoidCallback;
import com.accloud.service.ACAccountMgr;
import com.accloud.service.ACException;
import com.accloud.service.ACUserInfo;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{

    ACAccountMgr accountMgr=AC.accountMgr();


    private EditText Account;
    private EditText Pwd;
    private Button loginBtn;
    private Button registerBtn;
    private Button lgoutBtn;
    private String account;
    private  String pwd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView(){
        loginBtn  = (Button)findViewById(R.id.loginBtn);
        registerBtn=(Button)findViewById(R.id.btnRegister1);
        Account=(EditText)findViewById(R.id.Account);
        Pwd=(EditText)findViewById(R.id.Pwd);
        lgoutBtn=(Button)findViewById(R.id.btnLgout);


        loginBtn.setOnClickListener(this);
        registerBtn.setOnClickListener(this);
        lgoutBtn.setOnClickListener(this);
    }

    private void login(String account,String pwd){
        accountMgr.login(account, pwd, new PayloadCallback<ACUserInfo>() {
            @Override
            public void success(ACUserInfo acUserInfo) {
                Log.i("mytag","登录成功");
            }

            @Override
            public void error(ACException e) {
                Log.i("mytag","登录失败");
            }
        });
    }

    @Override
    public void onClick(View v) {

        account =Account.getText().toString();
        pwd=Pwd.getText().toString();

        Intent intent=null;
        switch (v.getId()){
            case R.id.loginBtn:
                if(accountMgr.isLogin()){
               Log.i("mytag","已登录");

                }else{
                    login(account,pwd);
                }
                intent=new Intent(MainActivity.this,HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.btnRegister1:
                intent=new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLgout:
                Log.i("mytag","注销");
                accountMgr.logout();
                break;


        }

    }

}
