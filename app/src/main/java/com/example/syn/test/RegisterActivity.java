package com.example.syn.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.cloudservice.VoidCallback;
import com.accloud.service.ACAccountMgr;
import com.accloud.service.ACException;
import com.accloud.service.ACUserInfo;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etAccount;
    private EditText etPwd;
    private EditText etVerifyCode;
    private Button btnVerifyCode;
    private Button btnRegister;
    private Button btnBack;

    private String account;
    private String pwd;
    private String verifyCode;


    ACAccountMgr acAccountMgr;
    private boolean flag=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }

    private void initView()
    {
        acAccountMgr= AC.accountMgr();
        etAccount=(EditText)findViewById(R.id.etAccount);
        etPwd=(EditText)findViewById(R.id.etPwd);
        etVerifyCode=(EditText)findViewById(R.id.etVerifyCode);
        btnVerifyCode=(Button)findViewById(R.id.btnVerifyCode);
        btnRegister=(Button)findViewById(R.id.btnRegister2);
        btnBack=(Button)findViewById(R.id.btnBack);
        btnVerifyCode.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }

    private void sendVerifyCode(String account){
        acAccountMgr.sendVerifyCode(account, 1, new VoidCallback() {
            @Override
            public void success() {
                Log.i("mytag","验证码发送成功");
            }

            @Override
            public void error(ACException e) {
                Log.i("mytag","验证码发送失败");
            }
        });

    }

    private void checkCode(String account,String verifyCode){
        acAccountMgr.checkVerifyCode(account, verifyCode, new PayloadCallback<Boolean>() {
            @Override
            public void success(Boolean aBoolean) {
                Log.i("mytag","验证码正确");
            }

            @Override
            public void error(ACException e) {
                Log.i("mytag","验证码错误");
            }
        });

    }


    private void checkAccount(String account){
        acAccountMgr.checkExist(account, new PayloadCallback<Boolean>() {
            @Override
            public void success(Boolean isExist) {
                if(!isExist){
                    flag=true;
                    Log.i("mytag","账号不存在，允许注册");

                }
                else{
                    flag=false;
                    Log.i("mytag","手机号已注册，不可重复注册");
                }
            }
            @Override
            public void error(ACException e) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        account=etAccount.getText().toString();
        pwd=etPwd.getText().toString();
        verifyCode=etVerifyCode.getText().toString();
        switch (v.getId()){

            case R.id.btnVerifyCode:
                checkAccount(account);
                if(flag) {
                    sendVerifyCode(account);
                }else{
                    return;
                }
                break;

            case R.id.btnRegister2:
                checkCode(account,verifyCode);
                acAccountMgr.register("", account, pwd, "", verifyCode
                        , new PayloadCallback<ACUserInfo>() {
                            @Override
                            public void success(ACUserInfo acUserInfo) {
                                Log.i("mytag","注册成功");
                            }

                            @Override
                            public void error(ACException e) {
                                Log.i("mytag","注册失败");
                            }
                        });
                break;

            case R.id.btnBack:
                Log.i("mytag","返回");
                Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
                startActivity(intent);
                RegisterActivity.this.finish();
                break;



        }
    }
}
