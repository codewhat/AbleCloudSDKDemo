package com.example.syn.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.service.ACDeviceDataMgr;
import com.accloud.service.ACException;
import com.accloud.service.ACMsg;
import com.accloud.service.ACObject;
import com.accloud.service.ACUserDevice;
import com.accloud.service.Criteria;
import com.accloud.service.Expression;
import com.accloud.service.QueryOption;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SYN on 2017/11/28.
 */
public class HomeActivity extends Activity implements View.OnClickListener {

    private Button btnGetDevice;
    private Button btnGetUser;
    private  Button btnAddGroup;
    private Button btnDeleteGroup;
    private Button TestBtn;
    ACDeviceDataMgr deviceDataMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);
        deviceDataMgr = AC.deviceDataMgr();
        btnGetDevice = (Button) findViewById(R.id.btngetDevice);
        btnGetUser = (Button) findViewById(R.id.btngetUser);
        btnAddGroup= (Button) findViewById(R.id.btnaddGroup);
        btnDeleteGroup=(Button)findViewById(R.id.btndeleteGroup);
        TestBtn = (Button)findViewById(R.id.TestBtn);

        btnGetDevice.setOnClickListener(this);
        btnGetUser.setOnClickListener(this);
        btnAddGroup.setOnClickListener(this);
        btnDeleteGroup.setOnClickListener(this);
        TestBtn.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent intent=null;
        switch (v.getId()){
            case R.id.btngetDevice:
                intent=new Intent(HomeActivity.this,ProductActivity.class);
                startActivity(intent);
                break;
            case R.id.btngetUser:
               intent=new Intent(HomeActivity.this,UserActivity.class);
                startActivity(intent);
                break;
            case R.id.btnaddGroup:
                intent = new Intent(HomeActivity.this,AddGroupActivity.class);
                startActivity(intent);
                break;
            case R.id.btndeleteGroup:
                intent=new Intent(HomeActivity.this,DeleteGroupActivity.class);
                startActivity(intent);
                break;
            case R.id.TestBtn:
               // senToService();
               // binddevice();
                fetchHistory();
                break;

        }




    }

    private void fetchHistory() {
        Criteria criteria=Criteria.create();
        QueryOption option=new QueryOption(criteria);


        AC.deviceDataMgr().fetchHistoryProperty("demo",49,option,
            new PayloadCallback<List<String>>(){
                @Override
                public void success(List<String> strings) {
                    ArrayList<CustomPropertyRecord> list=new ArrayList<CustomPropertyRecord>();
                    if(strings!=null){
                        Gson gson=new Gson();
                        for(String string:strings){
                            Log.i("mytag","设备属性："+string);
                            list.add(gson.fromJson(string,CustomPropertyRecord.class));
                        }
                    }
                }
                @Override
                public void error(ACException e) {
                    Log.i("mytag","拉取设备属性历史数据失败");
                }
            });


    }

    private void binddevice() {
        AC.bindMgr().bindDevice("demo", "864811036813355", "newDevice", new PayloadCallback<ACUserDevice>() {
            @Override
            public void success(ACUserDevice acUserDevice) {
                Log.i("mytag",acUserDevice.toString()+"绑定成功");
            }

            @Override
            public void error(ACException e) {
                Log.i("mytag",e+"绑定失败");
            }
        });

    }

    private void senToService() {
            ACMsg req = new ACMsg();
            req.setName("exportHistoryd");
            req.put("subDomainName","demo");
            req.put("deviceId","50");

            AC.sendToService("DEVICEDEMO", 2, req, new PayloadCallback<ACMsg>() {
                @Override
                public void success(ACMsg acMsg) {
                    Log.i("mytag","测试成功");
                   /* ArrayList<ACObject> deviceData= (ArrayList<ACObject>) acMsg.get("deviceList"); //设备列表数据，包括设备Id 和 设备所对应的组号
                    groupIdData=acMsg.get("groupIds");  //获取所登录用户所在的所有分组
                    userData= (ArrayList<ACObject>) acMsg.get("userList");
                    Log.i("mytag",deviceData.toString());
                    deviceList.setAdapter(new Adapter(getApplicationContext(),deviceData,groupIdData));
                     */
                    ACObject deviceData=acMsg.get("deviceData");
                    Log.i("mytag",deviceData.toString());

                }
                @Override
                public void error(ACException e) {

                    Log.i("mytag","测试失败"+e);
                }
            });


    }


}
